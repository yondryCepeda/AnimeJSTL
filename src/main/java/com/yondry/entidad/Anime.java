package com.yondry.entidad;

import java.io.Serializable;

public class Anime implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7101234739778103371L;
	private String nombreAnime;
	private String nombrePersonajePrincipal;
	
	
	public String getNombreAnime() {
		return nombreAnime;
	}
	public void setNombreAnime(String nombreAnime) {
		this.nombreAnime = nombreAnime;
	}
	public String getNombrePersonajePrincipal() {
		return nombrePersonajePrincipal;
	}
	public void setNombrePersonajePrincipal(String nombrePersonajePrincipal) {
		this.nombrePersonajePrincipal = nombrePersonajePrincipal;
	}
	@Override
	public String toString() {
		return "Anime [nombreAnime=" + nombreAnime + ", nombrePersonajePrincipal=" + nombrePersonajePrincipal + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombreAnime == null) ? 0 : nombreAnime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anime other = (Anime) obj;
		if (nombreAnime == null) {
			if (other.nombreAnime != null)
				return false;
		} else if (!nombreAnime.equals(other.nombreAnime))
			return false;
		return true;
	}
	
	
	
}
