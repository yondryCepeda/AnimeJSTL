package com.yondry.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.yondry.entidad.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AnimeServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 812215528823307787L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Anime a1 =new Anime();
		a1.setNombreAnime("Dragon Ball Super");
		a1.setNombrePersonajePrincipal("Goku");
		
		Anime a2 =new Anime();
		a2.setNombreAnime("One Puch Man");
		a2.setNombrePersonajePrincipal("Saitama");
		
		Anime a3 =new Anime();
		a3.setNombreAnime("Ataque a los titanes");
		a3.setNombrePersonajePrincipal("Eren");
		
		Anime a4 =new Anime();
		a4.setNombreAnime("Naruto");
		a4.setNombrePersonajePrincipal("Naruto Uzumaki");
		
		
		List <Anime> listaAnime = new ArrayList();
		
		listaAnime.add(a1);
		listaAnime.add(a2);
		listaAnime.add(a3);
		listaAnime.add(a4);
		
		//System.out.println(listaAnime);
		
		req.setAttribute("listaAnime", listaAnime);
		req.getRequestDispatcher("/jstl/anime.jsp").forward(req, resp);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
